#! /usr/bin/env node

const nodeGetopt = require('node-getopt');
const cloudbox = require('../lib/cloudbox');

var help =
    "\nUsage:" +
    "\n  cloudbox <action> [args]" +
    "\n\nOptions:" +
    "\n[[OPTIONS]]\n";

var opts = [
    ['h', 'help', 'Displays this help']
];

var getopt = nodeGetopt.create(opts).setHelp(help).bindHelp();
var opts = getopt.parseSystem();

function usage(message) {
    if (message) {
        console.info(message);
    }

    getopt.showHelp();
    process.exit();
}

if (opts.argv.length < 1) {
    usage();
}

var action = opts.argv.shift();

try {
    cloudbox.run(action, opts.argv, opts.options);
} catch(e) {
    usage(e.message);
}
