# CloudBox

CloudBox is a command-line tool to launch aws ec2 instances from set configuration.

## Installation

``` bash
# Install the cloudbox cli tool
$ npm install -g cloudbox
```

## Usage

```bash
# View help
$ cloudbox -h

# Launch an instance for the myproject project
$ cloudbox up myproject

# List instances for all projects
$ cloudbox list

# List instancs for the myproject project
$ cloudbox list myproject

# Terminate an instance by hostname
$ cloudbox down hostname.mydomain.tld

# Terminate all instances for the myproject project
$ cloudbox down myproject
```

## Example `cloudbox.json`

The `cloudbox.json` file defines the project configuration, and should exist within the current working directory.

Configuration is defined for each cloudbox project, and one or more ec2 instances may be launched against that project.

```json
{
    "projects": {
        "myproject": {
            "ami": "ami-aaaa1234",
            "instanceType": "t2.nano",
            "hostedZoneId": "Z1234",
            "hostname": "myproject.dev.mydomain.tld",
            "subnetId": "subnet-1234",
            "securityGroupId": "sg-1234",
            "init": "/opt/mycompany/init",
            "keyName": "dev",
            "tags": {
                "cloudpirate.app": "myproject-web",
                "cloudpirate.env": "dev"
            }
        }
    }
}
```

### Options

| Option          | Description                                                                                                               |
| --------------- | ------------------------------------------------------------------------------------------------------------------------- |
| ami             | The AMI id to use when launching the ec2 instance.                                                                        |
| instanceType    | The ec2 instance type.                                                                                                    |
| hostedZoneId    | The route53 hosted zone id of where to create the new DNS A record.                                                       |
| hostname        | The hostname suffix for the new DNS A record. This should match the domain name of the hostedZoneId.                      |
| subnetId        | The VPC subnet id to launch the new ec2 instance.                                                                         |
| securityGroupId | The security group id to attach to the ec2 instance.                                                                      |
| init            | An init script to run when the ec2 instance becomes available.                                                            |
| keyName         | The key pair name to attach to the ec2 instance to enable SSH access.                                                     |
| tags            | A list of tags for the ec2 instance, the example shows tags for [cloudpirate](https://www.npmjs.com/package/cloudpirate). |

## Aws

In order to launch ec2 instances, the following should exist.

- A route53 hosted zone where DNS A records can be created.
- A public subnet within a VPC to launch ec2 instances in to.
- A security group within the VPC to allow relevant incomming ports, typically 80 and 22.
- An AMI for the ec2 instance to launch from, which also includes an init script to invoke.
- A key pair to enable SSH access.

### Configuration

Aws configuration, resides within the file `cloudbox-aws.json` in the current working directory.

The IAM user should have full privileges for ec2 instances.

``` json
{
    "accessKeyId": "<access key>",
    "secretAccessKey": "<secret access key>",
    "region": "<region>"
}
```

The aws credentials may be omitted from this file, in favour of the environment variables `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`.

## Usage with CloudPirate

CloudBox may be used with [cloudpirate](https://www.npmjs.com/package/cloudpirate) so that further code deployments may be made.

To achieve this, the following should be in place.

- A application or environment configuration should exist within cloudpirate per cloudbox project.
- cloudpirate tags should be configured within cloudbox. i.e. `cloudpirate.app` and `cloudpirate.env`.
- A key pair name should be used that exists both within the cloudbox and cloudpirate configuration.

#### License: MIT
#### Author: [Ash Brown](https://bitbucket.org/bedican)
