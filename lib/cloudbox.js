var Config = require('./config');

var actions = {
    up: require('./actions/up'),
    down: require('./actions/down'),
    list: require('./actions/list')
};

module.exports = {
    run: function(action, args, opts) {

        if (!actions[action]) {
            throw new Error('Unkown action: ' + action);
        }

        var config = new Config(process.cwd() + '/cloudbox.json');

        actions[action].run(config, args, opts);
    }
};
