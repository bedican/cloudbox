const AWS = require('aws-sdk');
const fs = require('fs');

module.exports = {
    config: function() {
        try {
            return JSON.parse(fs.readFileSync(process.cwd() + '/cloudbox-aws.json', 'utf8'));
        } catch (e) {
            throw new Error('Failed to read aws configuration');
        }
    },
    ec2: function() {
        AWS.config.update(this.config());
        return new AWS.EC2();
    },
    route53: function() {
        AWS.config.update(this.config());
        return new AWS.Route53();
    }
};
