const fs = require('fs');

var Config = function(filename) {
    this.config = JSON.parse(fs.readFileSync(filename, 'utf8'));
};

Config.prototype.isProject = function(project) {
    return (this.config.projects[project]) ? true : false;
};

Config.prototype.getProjects = function() {
    if (!this.config.projects) {
        return [];
    }

    return Object.keys(this.config.projects);
};

Config.prototype.getProjectConfig = function(project) {
    if (!this.config.projects[project]) {
        return false;
    }

    return new ProjectConfig(project, this.config.projects[project]);
};

var ProjectConfig = function(project, config) {
    this.project = project;
    this.config = config;
};

ProjectConfig.prototype.getProject = function() {
    return this.project;
};

ProjectConfig.prototype.getAmi = function() {
    return this.config.ami || false;
};

ProjectConfig.prototype.getHostname = function() {
    return this.config.hostname || false;
};

ProjectConfig.prototype.getHostedZoneId = function() {
    return this.config.hostedZoneId || false;
};

ProjectConfig.prototype.getInit = function() {
    return this.config.init || false;
};

ProjectConfig.prototype.getTags = function() {
    return this.config.tags || false;
};

ProjectConfig.prototype.getInstanceType = function() {
    return this.config.instanceType || false;
};

ProjectConfig.prototype.getSubnetId = function() {
    return this.config.subnetId || false;
};

ProjectConfig.prototype.getSecurityGroupId = function() {
    return this.config.securityGroupId || false;
};

ProjectConfig.prototype.getKeyName = function() {
    return this.config.keyName || false;
};

module.exports = Config;
