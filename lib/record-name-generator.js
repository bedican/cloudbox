var names = [
    'zeus',
    'hera',
    'poseidon',
    'demeter',
    'ares',
    'athena',
    'apollo',
    'artemis',
    'hephaestus',
    'aphrodite',
    'hermes',
    'dionysus',
    'hades',
    'hypnos',
    'nike',
    'janus',
    'nemesis',
    'iris',
    'hecate',
    'tyche'
];

module.exports = {
    generate: function(excludeNames) {
        var available = names.filter(function(i) {
            return excludeNames.indexOf(i) == -1;
        });

        if (!available.length) {
            return (new Date()).getTime();
        }

        return available[Math.floor(Math.random() * available.length)];
    }
};
