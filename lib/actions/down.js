const factory = require('../aws-service-factory');

const ec2 = factory.ec2();
const route53 = factory.route53();

function getInstances(chain)
{
    return new Promise(function(resolve, reject) {

        var params = {
            Filters: [
                {
                    Name: 'instance-state-name',
                    Values: ['running']
                }
            ]
        };

        if (chain.project) {
            params.Filters.push({
                Name: 'tag:cloudbox.project',
                Values: [chain.project]
            });
        }

        if (chain.hostname) {
            params.Filters.push({
                Name: 'tag:cloudbox.hostname',
                Values: [chain.hostname]
            });
        }

        ec2.describeInstances(params, function(err, data) {
            if (err) { reject(err); return; }

            var reservation, instance, tags;
            var instances = [];

            for (var x in data.Reservations) {
                reservation = data.Reservations[x];
                for (var y in reservation.Instances) {
                    instance = reservation.Instances[y];

                    tags = {};
                    for(var t in instance.Tags) {
                        tags[instance.Tags[t].Key] = instance.Tags[t].Value;
                    }

                    instances.push({
                        id: instance.InstanceId,
                        ip: instance.PublicIpAddress,
                        hostname: tags['cloudbox.hostname'],
                        project: tags['cloudbox.project']
                    });
                }
            }

            chain.instances = instances;

            resolve(chain);
        });
    });
}

function terminateInstances(chain)
{
    return new Promise(function(resolve, reject) {

        if (!chain.instances.length) {
            resolve(chain); return;
        }

        var instanceIds = [];

        for (var key in chain.instances) {
            instanceIds.push(chain.instances[key].id);
        }

        var params = {
            InstanceIds: instanceIds
        };

        console.log("Terminating instances...");

        ec2.terminateInstances(params, function(err, data) {
            if (err) { reject(err); return; }

            var instances = [];
            for(var key in data.TerminatingInstances) {
                instances.push(data.TerminatingInstances[key].InstanceId);
            }

            console.log("Requested termination for", instances);

            resolve(chain);
        });
    });
}

function removeDns(config, chain)
{
    return new Promise(function(resolve, reject) {

        if (!chain.instances.length) {
            resolve(chain); return;
        }

        // There will be 1 or more instances, of which all have the same project.
        config = config.getProjectConfig(chain.instances[0].project);
        if (!config) {
            resolve(chain); return;
        }

        var params = {
            HostedZoneId: config.getHostedZoneId(),
            ChangeBatch: {
                Changes: []
            }
        };

        for (var key in chain.instances) {
            params.ChangeBatch.Changes.push({
                Action: 'DELETE',
                ResourceRecordSet: {
                    Name: chain.instances[key].hostname,
                    Type: 'A',
                    TTL: 300,
                    ResourceRecords: [
                        {
                            Value: chain.instances[key].ip
                        }
                    ]
                }
            });
        }

        console.log("Removing dns...");

        route53.changeResourceRecordSets(params, function(err, data) {
            if (err) { reject(err); return; }

            console.log("Removed dns");
            resolve(chain);
        });
    });
}

module.exports = {
    run: function(config, args, opts) {

        var projectOrHost = args.shift();

        if (!projectOrHost) {
            throw new Error('No project or hostname specified');
        }

        var chain = {};

        if (config.isProject(projectOrHost)) {
            chain.project = projectOrHost;
        } else {
            chain.hostname = projectOrHost;
        }

        getInstances(chain)
            .then(function(response) {
                return terminateInstances(response);
            })
            .then(function(response) {
                return removeDns(config, response);
            })
            .catch(function(err) {
                console.log(err.stack);
            });
    }
};
