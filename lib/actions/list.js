const Table = require('cli-table');
const colors = require('colors');
const factory = require('../aws-service-factory');

const ec2 = factory.ec2();

function getInstances(project, chain) {

    return new Promise(function(resolve, reject) {

        var params = {
            Filters: [
                {
                    Name: 'instance-state-name',
                    Values: ['running']
                },
                {
                    Name: 'tag-key',
                    Values: ['cloudbox.project']
                }
            ]
        };

        if (project) {
            params.Filters.push({
                Name: 'tag:cloudbox.project',
                Values: [project]
            });
        }

        ec2.describeInstances(params, function(err, data) {
            if (err) { reject(err); return; }

            var reservation, instance, tags;
            var instances = [];

            for (var x in data.Reservations) {
                reservation = data.Reservations[x];
                for (var y in reservation.Instances) {
                    instance = reservation.Instances[y];

                    tags = {};
                    for(var t in instance.Tags) {
                        tags[instance.Tags[t].Key] = instance.Tags[t].Value;
                    }

                    instances.push({
                        id: instance.InstanceId,
                        ip: instance.PublicIpAddress,
                        type: instance.InstanceType,
                        time: instance.LaunchTime,
                        tags: tags
                    });
                }
            }

            chain.instances = instances;

            resolve(chain);
        });
    });
}

function displayInstances(chain) {

    return new Promise(function(resolve, reject) {

        var consoleWidth = process.stdout.getWindowSize()[0];

        var table = new Table({
            head: ['ID'.bold.cyan, 'Project'.bold.cyan, 'Type'.bold.cyan, 'Up Since'.bold.cyan, 'Host'.bold.cyan],
            colWidths: [
                parseInt((consoleWidth / 100) * 10),
                parseInt((consoleWidth / 100) * 15),
                parseInt((consoleWidth / 100) * 10),
                parseInt((consoleWidth / 100) * 30),
                parseInt((consoleWidth / 100) * 30)
            ],
            truncate: false
        });

        for(var key in chain.instances) {
            table.push(
                [
                    chain.instances[key].id,
                    chain.instances[key].tags['cloudbox.project'],
                    chain.instances[key].type,
                    chain.instances[key].time,
                    chain.instances[key].tags['cloudbox.hostname']
                ]
            );
        }

        console.log(table.toString());

        resolve();
    });
}

module.exports = {
    run: function(config, args, opts) {

        var project = args.shift();
        var chain = {};

        getInstances(project, chain)
            .then(function(response) {
                return displayInstances(response);
            })
            .catch(function(err) {
                console.log(err.stack);
            });
    }
};
