const factory = require('../aws-service-factory');
const recordNameGenerator = require('../record-name-generator');

const ec2 = factory.ec2();
const route53 = factory.route53();

function runInstance(config, chain) {
    return new Promise(function(resolve, reject) {

        var userData = new Buffer([
            "#!/bin/bash",
            "sudo -u ubuntu " + config.getInit()
        ].join("\n"));

        var params = {
            ImageId: config.getAmi(),
            InstanceType: config.getInstanceType(),
            MinCount: 1,
            MaxCount: 1,
            NetworkInterfaces: [
                {
                    DeviceIndex: 0,
                    AssociatePublicIpAddress: true,
                    SubnetId: config.getSubnetId(),
                    Groups: [ config.getSecurityGroupId() ]
                }
            ],
            UserData: userData.toString('base64'),
            KeyName: config.getKeyName()
        };

        console.log("Creating instance...");

        ec2.runInstances(params, function(err, ec2data) {
            if (err) { reject(err); return; }

            chain.ec2data = ec2data;

            var instanceId = ec2data.Instances[0].InstanceId;
            console.log("Created instance", instanceId);

            resolve(chain);
        });
    });
}

function waitForInstanceRunning(config, chain) {
    return new Promise(function(resolve, reject) {
        var params = {
            InstanceIds: [chain.ec2data.Instances[0].InstanceId]
        };

        console.log("Waiting for instance running state...");

        ec2.waitFor('instanceRunning', params, function(err, data) {
            if (err) { reject(err); return; }

            chain.ec2data = data.Reservations[0];

            console.log("Instance running");
            resolve(chain);
        });
    });
}

function generateRecordName(config) {
    return new Promise(function(resolve, reject) {

        var params = {
            HostedZoneId: config.getHostedZoneId()
        };

        console.log("Generating record name...");

        route53.listResourceRecordSets(params, function(err, data) {
            if (err) { reject(err); return; }

            var suffix = config.getHostname();

            var hostname;
            var recordNames = [];

            for (record in data.ResourceRecordSets) {
                if (data.ResourceRecordSets[record].Type != 'A') {
                    continue;
                }

                hostname = data.ResourceRecordSets[record].Name.replace(/\.$/g, '');

                if (!hostname.endsWith('.' + suffix)) {
                    continue;
                }

                recordNames.push(hostname.substr(0, hostname.length - suffix.length - 1));
            }

            var recordName = recordNameGenerator.generate(recordNames);

            if (!recordName) {
                reject(new Error('Could not generate new record name')); return;
            }

            recordName += '.' + suffix;

            console.log("Record name generated", recordName);
            resolve(recordName);
        });
    });
}

function createDns(config, chain) {
    return new Promise(function(resolve, reject) {

        generateRecordName(config).then(function(recordName) {

            chain.recordName = recordName;

            var params = {
                HostedZoneId: config.getHostedZoneId(),
                ChangeBatch: {
                    Changes: [
                        {
                            Action: 'CREATE',
                            ResourceRecordSet: {
                                Name: recordName,
                                Type: 'A',
                                TTL: 300,
                                ResourceRecords: [
                                    {
                                        Value: chain.ec2data.Instances[0].PublicIpAddress
                                    }
                                ]
                            }
                        }
                    ]
                }
            };

            console.log("Creating dns...");

            route53.changeResourceRecordSets(params, function(err, data) {
                if (err) { reject(err); return; }

                console.log("Created dns");
                resolve(chain);
            });

        }).catch(function(err) {
            reject(err);
        });
    });
}

function tagInstance(config, chain) {
    return new Promise(function(resolve, reject) {
        var params = {
            Resources: [chain.ec2data.Instances[0].InstanceId],
            Tags: [
                { Key: 'Name', Value: 'cloudbox: ' + config.getProject() },
                { Key: 'cloudbox.project', Value: config.getProject() },
                { Key: 'cloudbox.hostname', Value: chain.recordName }
            ]
        };

        var tags = config.getTags();
        for (var name in tags) {
            params.Tags.push({ Key: name, Value: tags[name] });
        }

        console.log("Tagging instance...");

        ec2.createTags(params, function(err, data) {
            if (err) { reject(err); return; }

            console.log("Instance tagged");

            resolve(chain);
        });
    });
}

module.exports = {
    run: function(config, args, opts) {

        var project = args.shift();
        if (!project) {
            throw new Error('No project specified');
        }

        config = config.getProjectConfig(project);
        if (!config) {
            throw new Error('Unknown project: ' + project);
        }

        var chain = {};

        runInstance(config, chain)
            .then(function(response) {
                return waitForInstanceRunning(config, response);
            })
            .then(function(response) {
                return createDns(config, response);
            })
            .then(function(response) {
                return tagInstance(config, response);
            })
            .catch(function(err) {
                console.log(err.stack);
            });
    }
};
